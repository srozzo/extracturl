package main

import (
	"bufio"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"text/template"
)

const ianaTLDList = `https://data.iana.org/TLD/tlds-alpha-by-domain.txt`
const filePath = "tlds.go"

var tldTempl = template.Must(template.New("tlds").Parse(`package extracturl

/*
 * List of all of the ICANN/IANA recognized TLDS
 *
 * Source: https://data.iana.org/TLD/tlds-alpha-by-domain.txt
 * 
 */

var TLDs = []string{
{{range .}}` + "\t`" + `{{.}}` + "`" + `,` + "\n" + `{{end}}
}
`))

func main() {
	fmt.Printf("Creating %s...\n", filePath)

	if err := writeTlds(); err != nil {
		fmt.Println(err)
	}

	cmd := exec.Command("go", "fmt", filePath)
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Complete")
}

func readTlds() (*[]string, error) {
	var tlds []string

	// read remote resource
	resp, err := http.Get(ianaTLDList)
	if err != nil {
		return &tlds, err
	}
	if resp.StatusCode >= 400 {
		err = errors.New(resp.Status)
		return &tlds, err
	}
	defer resp.Body.Close()

	scanner := bufio.NewScanner(resp.Body)
	re := regexp.MustCompile(`^[^#]+$`)

	for scanner.Scan() {
		tld := strings.ToLower(re.FindString(scanner.Text()))
		if tld == `` {
			continue
		}

		tlds = append(tlds, tld)
	}
	return &tlds, nil
}

func writeTlds() error {
	tlds, err := readTlds()
	if err != nil {
		return err
	}

	f, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	tldTempl.Execute(f, tlds)

	return nil
}
