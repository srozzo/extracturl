package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"text/template"
)

const ianaSchemeList = `https://www.iana.org/assignments/uri-schemes/uri-schemes-1.csv`
const filePath = "schemes.go"

var schemeTempl = template.Must(template.New("schemes").Parse(`package extracturl

/*
 * List of all of the IANA registered URI schemes
 *
 * Reference: https://www.iana.org/assignments/uri-schemes/uri-schemes-1.csv
 * 
 */

var Schemes = []string{
{{range .}}` + "\t`" + `{{.}}` + "`" + `,` + "\n" + `{{end}}
}
`))

func main() {
	fmt.Printf("Creating %s...\n", filePath)

	if err := writeSchemes(); err != nil {
		fmt.Println(err)
	}

	cmd := exec.Command("go", "fmt", filePath)
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Complete")
}

func readSchemes() (*[]string, error) {
	var schemes []string

	// read remote resource
	resp, err := http.Get(ianaSchemeList)
	if err != nil {
		return &schemes, err
	}
	if resp.StatusCode >= 400 {
		err = errors.New(resp.Status)
		return &schemes, err
	}
	defer resp.Body.Close()
	csv := csv.NewReader(resp.Body)
	csv.Read() // Skip headers

	for {
		record, err := csv.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return &schemes, err
		}
		schemes = append(schemes, record[0])
	}

	return &schemes, nil
}

func writeSchemes() error {
	schemes, err := readSchemes()
	if err != nil {
		return err
	}

	f, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	schemeTempl.Execute(f, schemes)

	return nil
}
