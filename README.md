extracturl
=======

[![Build Status](https://img.shields.io/bitbucket/pipelines/srozzo/extracturl.svg)](https://bitbucket.org/srozzo/extracturl/addon/pipelines/home#!/)

Examples in ./examples

LICENSE
-------
extracturl is licensed under the Mozilla Public License 2.0(https://choosealicense.com/licenses/mpl-2.0/).


How to Install
--------------
```shell
$ go get -u bitbucket.org/srozzo/extracturl
```

Refreshing IANA Registered TLDs and Schemes
---
```
 go run generators/genSchemes.go
 go run generators/genTlds.go
```

Sample Program
--------------

```go
package main

import (
	"fmt"
	"os"

	"bitbucket.org/srozzo/extracturl"
)

func main() {
    document := `x.com the bsdh@foo.한국:343/foo little sdss@foo-23s.com sdls:sds@foo.bar.net car https://23.32.32.23:4040 https://www.google.com:6265/sjdso/sds/../sds.htm and http://www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument but then skds.net:34332/sadjha/asda.php?3223=22 and the www.bing.com has two monkeys`
    
	fmt.Println("Original Document:\n " + document + "\n")

	urls, err := extracturl.Extract(document)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	for _, v := range urls {
		fmt.Println("URI Found: " + v)
	}
}
```