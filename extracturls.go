package extracturl

import (
	"bytes"
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"unicode"

	"golang.org/x/net/idna"
)

// Extract takes a string and returns an array of all URLs found in the document
func Extract(document string) ([]string, error) {
	var err error
	var uris []string
	var words []string

	uriStrictExpr := getURIRegex(true)
	uriLooseExpr := getURIRegex(false)

	reStrict := regexp.MustCompile(uriStrictExpr)
	reLoose := regexp.MustCompile(uriLooseExpr)

	// Tokenize our words on whitespace
	words = splitString(document)

	// Iterate over words looking for URLs
	for _, word := range words {
		// Perform a loose regex since we haven't converted i18n domains to punycode
		if reLoose.MatchString(word) {
			word, err = convToPunycode(word)
			if err != nil {
				return uris, err
			}
		}

		// Do a strict regex lookup now that any i18n domains should be converted to punycode
		match := reStrict.MatchString(word)
		if match {
			uris = append(uris, word)
		}
	}

	return uris, nil
}

// convToPunycode converts any URL to its Punycode equivelent
func convToPunycode(s string) (string, error) {
	var match bool
	var err error
	var uri *url.URL
	var host []string
	var hostname string
	var port string
	var addedScheme bool

	// If a scheme is not present, we will assume it is http://
	match, err = regexp.MatchString(`^[\p{L}|\+|\.|\-]+://.*`, s)

	if err != nil {
		fmt.Println(err)
	}

	// If no scheme is specified, we will assume that this is a web url
	// We need a scheme for url.Parse to function as expected. We'll remove
	// It later
	addedScheme = false

	if !match {
		s = "http://" + s
		addedScheme = true
	}

	// Parse URL

	uri, err = url.Parse(s)
	if err != nil {
		return "", err
	}

	// Need to remove the port before passing the domain to the punycode convertor
	host = strings.Split(uri.Host, ":")
	hostname = host[0]

	// Capture the port if it was present
	if len(host) > 1 {
		port = host[1]
	}

	// Convert domain to punycode if applicable
	hostname, err = idna.ToASCII(hostname)
	if err != nil {
		return "", err
	}

	// Reconstruct the uri's host
	uri.Host = hostname
	if len(host) > 1 {
		uri.Host = uri.Host + ":" + port
	}

	res := uri.String()

	// If we added a scheme for the sake of url.Parse, let's get rid of it
	if addedScheme == true {
		re := regexp.MustCompile(`http://(.*)`)
		res = re.ReplaceAllString(res, "$1")
	}

	return res, nil
}

/*
 * splitString splits a string on all unicode space characters
 */
func splitString(s string) []string {
	var spaceRunes bytes.Buffer

	// Iterate over all unicode space points to build a regex
	for i, r := range unicode.Space.R16 {
		for c := r.Lo; c <= r.Hi; c += r.Stride {
			if i != 0 {
				spaceRunes.WriteString(` | `)
			}
			spaceRunes.WriteString(string(c))
		}
	}

	wordsRegex := regexp.MustCompile(`[`+spaceRunes.String()+`]`).Split(s, -1)

	return wordsRegex
}

/*
 * getURIRegex returns a loose or strict regular expression for matching URLs.
 */
func getURIRegex(strict bool) string {
	var uriSchemeExpr string
	var uriHostnameExpr string

	if strict {
		// Permit only IANA registered schemes
		uriSchemeExpr = `(?:` + getRegexAtomicGroupOr(Schemes...) + `://)?`
		// Permit only IANA registered TLDs
		uriHostnameExpr = `(?:[\p{L}\p{N}]+(?:(?:[\-])?[[\p{L}\p{N}]+)*?\.){1,}` + getRegexAtomicGroupOr(TLDs...)
	} else {
		// Permit none IANA registered schemes
		uriSchemeExpr = `(?:[\p{L}|\+\.\-]+://)?`
		// Permit any TLD
		uriHostnameExpr = `(?:[\p{L}\p{N}]+(?:(?:[\-])?[[\p{L}\p{N}]+)*?\.){1,}([\p{L}]+)`
	}

	// Regex is case insensitive
	reFlags := `(?i)`
	uriCredentialsExpr := `(:?\S+(:?:\S*)?@)?`
	uriIPExpr := `(?:(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3})`
	uriPortExpr := `(:[0-9]+)?`
	uriPathExp := `((\/|\?|#)[\S]*)?`

	uriExpr := reFlags + `\b(` + uriSchemeExpr + uriCredentialsExpr + `(?:` + uriHostnameExpr + `|` + uriIPExpr + `)` + uriPortExpr + uriPathExp + `)` + `\b`

	return uriExpr
}

/*
 * getRexegAtomicOr creates an atomic group of terms seperated by logical ORs
 */
func getRegexAtomicGroupOr(s ...string) string {
	var b bytes.Buffer

	b.WriteString(`(?:`)
	for itr, val := range s {
		if itr != 0 {
			b.WriteByte('|')
		}
		b.WriteString(regexp.QuoteMeta(val))
	}
	b.WriteByte(')')

	return b.String()
}
