package extracturl

import (
	"reflect"
	"testing"
)

func TestExtract(t *testing.T) {
	type args struct {
		document string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{`schemeless one letter domain`, args{`Go to x.com right now`}, []string{"x.com"}},
		{`schemeless complex domain`, args{`Click here skd3ksl-sdms-sds.xxx for fun`}, []string{"skd3ksl-sdms-sds.xxx"}},
		{`complex domain port, path`, args{`Look at https://www.phishing.com:6265/sjdso/sds/../sds.htm#haxed before you die`}, []string{"https://www.phishing.com:6265/sjdso/sds/../sds.htm#haxed"}},
		{`complex domain user, pass, port and path`, args{`Good times http://yourcreds:tastesogood@funonline.net:432/index.php#login found here`}, []string{"http://yourcreds:tastesogood@funonline.net:432/index.php#login"}},
		{`schemeless domain user, domain, port`, args{`Hey there how's about sdss@foo-23s.com this`}, []string{"sdss@foo-23s.com"}},
		{`littlebitofeverything`, args{`this is a long aaa://dsd:sds@www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument uri`}, []string{"aaa://dsd:sds@www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument"}},
		{`two domains`, args{`here is x.com and y.com`}, []string{"x.com", "y.com"}},
		{"international", args{`hi there click on my international http://aaa.한국/foo link here`}, []string{`http://aaa.xn--3e0b707e/foo`}},
		{"international complex", args{`blah blah http://aaa.한국:434543/foo#blarg link here`}, []string{`http://aaa.xn--3e0b707e:434543/foo#blarg`}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := Extract(tt.args.document); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Extract() = %v, want %v", got, tt.want)
			}
		})
	}
}
